package org.example.exception;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class ConstraintsValidationError {
    private String property;
    private String message;

}

