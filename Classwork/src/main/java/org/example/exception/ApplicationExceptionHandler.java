package org.example.exception;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.UnsupportedJwtException;
import jakarta.validation.ValidationException;
import lombok.RequiredArgsConstructor;
import org.example.service.TranslateService;
import org.springframework.context.MessageSource;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;

import java.nio.channels.AlreadyBoundException;
import java.nio.file.FileAlreadyExistsException;
import java.security.SignatureException;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@RestControllerAdvice
@RequiredArgsConstructor
public class ApplicationExceptionHandler {
    private final TranslateService translateService;
    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<ErrorResponseDto> productNotFound(NotFoundException e, WebRequest request) {
        String language = request.getHeader("Accept-Language");
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                ErrorResponseDto.builder().
                        status(404).
                        code(ErrorCode.ENTITY_NOT_FOUND_0001).
                        message(translateService.translate(e.getCode().getName(),language,e.getArgs())).
                        detail(translateService.translate(e.getCode().getName().concat("_DETAIL"),language,e.getArgs())).
                        path(((ServletWebRequest) request).getRequest().getRequestURI()).
                        requestLang(language).
                        build()
        );
    }
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public final ResponseEntity<ErrorResponseDto> methodArgumentNotValid(MethodArgumentNotValidException e, WebRequest request) {
        e.printStackTrace();
        List<ConstraintsValidationError> validationErrors = e.getBindingResult().
                getFieldErrors().
                stream().
                map(error->new ConstraintsValidationError(error.getField(),error.getDefaultMessage()))
                .collect(Collectors.toList());
        String language = request.getHeader("Accept-Language");
        ErrorResponseDto errorResponseDto = ErrorResponseDto.builder().
                status(HttpStatus.BAD_REQUEST.value()).
                code(ErrorCode.VALIDATION_ERROR).
                message(translateService.translate(ErrorCode.VALIDATION_ERROR.toString(), language)).
                detail(translateService.translate(ErrorCode.VALIDATION_ERROR.toString().concat("_DETAIL"), language)).
                path(((ServletWebRequest) request).getRequest().getRequestURI()).
                requestLang(language).
                build();
        validationErrors.forEach(
                validation->errorResponseDto.getData().put(validation.getProperty(),validation.getMessage()));
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponseDto);
    }
    @ExceptionHandler(HttpMessageNotReadableException.class)
    public final ResponseEntity<ErrorResponseDto> messageNotReadable(HttpMessageNotReadableException e, WebRequest request)
    {
        e.printStackTrace();
        String language = request.getHeader("Accept-Language");
        ErrorResponseDto errorResponseDto = ErrorResponseDto.builder().
                status(HttpStatus.BAD_REQUEST.value()).
                code(ErrorCode.HTTP_MESSAGE_NOT_READABLE).
                message(translateService.translate(ErrorCode.HTTP_MESSAGE_NOT_READABLE.getName(), language)).
                detail(translateService.translate(ErrorCode.HTTP_MESSAGE_NOT_READABLE.toString().concat("_DETAIL"), language)).
                path(((ServletWebRequest) request).getRequest().getRequestURI()).
                requestLang(language).
                build();
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponseDto);
    }
    @ExceptionHandler(DataIntegrityViolationException.class)
    public final ResponseEntity<ErrorResponseDto> dataIntegrityNotValid(DataIntegrityViolationException e, WebRequest request){
        e.printStackTrace();
        String language = request.getHeader("Accept-Language");
        ErrorResponseDto errorResponseDto = ErrorResponseDto.builder().
                status(HttpStatus.BAD_REQUEST.value()).
                code(ErrorCode.DATA_IS_EXIST).
                message(translateService.translate(ErrorCode.DATA_IS_EXIST.toString(), language)).
                detail(translateService.translate(ErrorCode.DATA_IS_EXIST.toString().concat("_DETAIL"), language)).
                path(((ServletWebRequest) request).getRequest().getRequestURI()).
                requestLang(language).
                build();
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponseDto);
    }
//    @ExceptionHandler(JwtException.class)
//    public ResponseEntity<ErrorResponseDto> handleJwtExceptions(JwtException e, WebRequest request) {
//        e.printStackTrace(); // You can replace this with proper logging
//
//        String language = request.getHeader("Accept-Language");
//        ErrorResponseDto errorResponseDto = ErrorResponseDto.builder()
//                .status(HttpStatus.UNAUTHORIZED.value())
//                .code(ErrorCode.JWT_NOT_VALID)
//                .message("Invalid JWT token") // Adjust this as per your message
//                .path(((ServletWebRequest) request).getRequest().getRequestURI())
//                .requestLang(language)
//                .build();
//
//        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(errorResponseDto);
//    }

}
