package org.example.exception;

import jakarta.annotation.Nullable;
import lombok.Data;

@Data
public class NotFoundException extends RuntimeException{
    ErrorCode code;
    private String[] args;


    public NotFoundException(ErrorCode code,@Nullable String ...args) {
        this.code = code;
        this.args = args;
    }

}
