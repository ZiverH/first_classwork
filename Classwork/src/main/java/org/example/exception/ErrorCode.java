package org.example.exception;

import lombok.Getter;

@Getter
public enum ErrorCode {
    ENTITY_NOT_FOUND_0001("ENTITY_NOT_FOUND_0001"),
    VALIDATION_ERROR("VALIDATION_ERROR"),
    HTTP_MESSAGE_NOT_READABLE("HTTP_MESSAGE_NOT_READABLE"),
    DATA_IS_EXIST("DATA_IS_EXIST"),
    JWT_NOT_VALID("JWT_NOT_VALID"),
    ACCESS_TOKEN_EXPIRED("ACCESS_TOKEN_EXPIRED")
    ;
    private final String name;
    private ErrorCode(String code) {
        this.name = code;
    }
}
