package org.example.exception;

import jakarta.annotation.Nullable;

public class ProductNotFoundException extends NotFoundException{

    public ProductNotFoundException(ErrorCode code,@Nullable String ...args) {
        super(code,args);
    }
}
