package org.example;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.example.model.*;
import org.example.repository.CategoriesRepository;
import org.example.repository.ProductDetailRepository;
import org.example.repository.ProductRepository;
import org.example.repository.ShoppingCartRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

import java.util.Optional;
import java.util.Set;

@SpringBootApplication
@RequiredArgsConstructor
@EnableCaching
public class ClassworkApplication implements CommandLineRunner {

    private final ProductRepository productRepository;
    private final CategoriesRepository categoriesRepository;
    private final ProductDetailRepository productDetailRepository;
    private final ShoppingCartRepository shoppingCartRepository;

    public static void main(String[] args) {
        SpringApplication.run(ClassworkApplication.class, args);
    }


    @Override
    public void run(String... args) throws Exception {


//        System.out.println(shoppingCartRepository.findByIdAll(5L));<Product> product = productRepository.findById(6L);
//        ProductDetail productDetail = ProductDetail.builder().
//                color("blue").
//                ImageUrl("img").
//                build();
//        Product product = Product.builder().
//                name("kitab2").
//                price(10).
//                category(Category.KITAB).
//                description("desc").
//                productDetail(productDetail).
//                build();
//        productRepository.save(product);
    }
}
