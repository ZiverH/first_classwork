package org.example.dto;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.example.model.Category;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ProductCreateDto {
    @NotNull
    @Size(min = 2, max = 50,message = "Product name must be greater than 2 and less than 50")
    String name;
    @NotNull
    @Min(1)
    Integer price;
    @NotNull
    Category category;
    String description;
    }
