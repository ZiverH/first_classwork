package org.example.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.example.model.Category;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ProductDto {
    Long id;
    String name;
    Integer price;
    Category category;
    String description;
//    Long product_detail_id;
    }
