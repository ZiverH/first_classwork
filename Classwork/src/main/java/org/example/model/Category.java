package org.example.model;

import java.io.Serializable;

public enum Category implements Serializable {
    KITAB,
    ELEKTRONIKA,
    MEISET
}
