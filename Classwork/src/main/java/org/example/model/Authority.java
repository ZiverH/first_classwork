package org.example.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Embeddable
@Builder
public class Authority implements GrantedAuthority {

    @Enumerated(EnumType.STRING)
    Role authority;

    public String getAuthority() {
        return authority.name();
    }
}
