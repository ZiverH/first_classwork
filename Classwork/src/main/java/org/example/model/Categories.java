package org.example.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
public class Categories {
    private static final long serialVersionUID = 110L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    String categoryname;
    @OneToMany(cascade = CascadeType.ALL,mappedBy = "categories")
    @ToString.Exclude
    @JsonIgnore
    @EqualsAndHashCode.Exclude
    Set<Product> products = new HashSet<>();
    
}
