package org.example.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;

import java.io.Serial;
import java.io.Serializable;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
public class Shopping_cart implements Serializable {
    private static final long serialVersionUID = 111L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    String name;
    @ManyToMany
    @JoinTable(
            name = "product_shopping_cart",
            joinColumns = @JoinColumn(name = "shopping_cart_id",referencedColumnName = "id"),
            inverseJoinColumns  = @JoinColumn(name = "product_id",referencedColumnName = "id")
    )
    Set<Product> products;
}