package org.example.model;


import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
public class ProductDetail implements Serializable {
       private static final long serialVersionUID = 11L;

       @Id
       @GeneratedValue(strategy = GenerationType.IDENTITY)
       Long id;
       String color;
       String ImageUrl;

}
