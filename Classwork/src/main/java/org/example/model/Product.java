package org.example.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "product", uniqueConstraints = {@UniqueConstraint(columnNames = "name")})
public class Product implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    String name;
    Integer price;
    @Enumerated(EnumType.STRING)
    Category category;
    String description;
    @OneToOne(cascade=CascadeType.ALL,fetch = FetchType.LAZY)
    ProductDetail productDetail;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "category_id")
    Categories categories;


//    @ManyToMany
//    @JoinTable(
//            name = "product_shopping_cart",
//            joinColumns = @JoinColumn(name = "product_id",referencedColumnName = "id"),
//            inverseJoinColumns  = @JoinColumn(name = "shopping_cart_id",referencedColumnName = "id")
//            )
//    Set<Shopping_cart> shopping_carts;

}
