package org.example.service;

import lombok.RequiredArgsConstructor;
import org.example.model.Shopping_cart;
import org.example.repository.ProductRepository;
import org.example.repository.ShoppingCartRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ShoppingCartCacheService {
    private final ShoppingCartRepository shoppingCartRepository;
    private final ProductRepository productRepository;

    @CachePut(cacheNames = "shoppingcart",key = "#result.id")
    public Shopping_cart createShoppingCart(String name) {
        Shopping_cart shoppingCart = Shopping_cart.builder().name(name).build();
        Shopping_cart save = shoppingCartRepository.save(shoppingCart);
        return save;
    }

    @CachePut(cacheNames = "shopping-cart",key = "#id")
    public Shopping_cart addProductToCart(Long id,Long productId){
        Shopping_cart shoppingCart = shoppingCartRepository.findById(id).get();
         if(!shoppingCart.getProducts().contains(productId)){
             shoppingCart.getProducts().add(productRepository.findById(productId).get());
             shoppingCartRepository.save(shoppingCart);
         }


        return shoppingCart;
    }
    @CacheEvict(cacheNames = "shopping-cart",key ="#id")
    public void removeProductFromCart(Long id){
        Shopping_cart shoppingCart = shoppingCartRepository.findById(id).get();
        shoppingCartRepository.save(shoppingCart);
    }
    @Cacheable(cacheNames = "shopping-cart",key ="#id")
    public Shopping_cart getShoppingCartById(Long id){
            return shoppingCartRepository.findById(id).get();
    }



}
