package org.example.service;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import lombok.RequiredArgsConstructor;
import org.example.config.BaseJwtService;
import org.example.dto.UserDto;
import org.example.exception.ErrorCode;
import org.example.exception.NotFoundException;
import org.example.model.Authority;
import org.example.model.User;
import org.example.repository.UserRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.*;

import static org.example.exception.ErrorCode.ENTITY_NOT_FOUND_0001;
import static org.example.model.Role.USER;

@Service
@RequiredArgsConstructor
public class UserService implements UserDetailsService {

    private final UserRepository userRepository;
    private final BCryptPasswordEncoder passwordEncoder;
    private final BaseJwtService baseJwtService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        var user= userRepository.findByUsername(username).orElseThrow(()->new NotFoundException(ErrorCode.ENTITY_NOT_FOUND_0001));
            return user;
    }

    public void createUser(UserDto userDto) {
        userRepository.findByUsername(userDto.getUsername()).ifPresentOrElse(
                user -> {
                    throw new IllegalArgumentException("User already exists");
                },
                () -> {
                    User user = User.builder()
                            .username(userDto.getUsername())
                            .password(passwordEncoder.encode(userDto.getPassword()))
                            .authorities(List.of(Authority.builder()
                                    .authority(USER)
                                    .build()))
                            .build();
                    userRepository.save(user);
                });
    }



    public Map<String,String> login(UserDto userDto) {
        User user = userRepository.findByUsername(userDto.getUsername()).orElseThrow(() -> new NotFoundException(ENTITY_NOT_FOUND_0001));
        boolean matches = passwordEncoder.matches(userDto.getPassword(), user.getPassword());
        if (!matches) {
            throw new IllegalArgumentException();
        }
        String accessToken = baseJwtService.accessToken(user);
        String refreshToken = baseJwtService.refreshToken(user);
        Map<String, String> tokens = new HashMap<>();
        tokens.put("accessToken", accessToken);
        tokens.put("refreshToken", refreshToken);
        return tokens;
    }

    public Map<String,String> refreshToken(Map<String,String> refreshtoken) {
        Jws<Claims> claimsJws = baseJwtService.parseJwt(refreshtoken.get("refreshToken"));
        String username = claimsJws.getPayload().get("sub").toString();
        User user = userRepository.findByUsername(username).orElseThrow(() -> new NotFoundException(ENTITY_NOT_FOUND_0001));
        Map<String,String> tokens = new HashMap<>();
        tokens.put("accessToken", baseJwtService.accessToken(user));
        tokens.put("refreshToken", baseJwtService.refreshToken(user));
        return tokens;
    }


    public void setUUID() {
        List<User> all = userRepository.findAll();
        for (User user : all) {
            if(user.isEnabled()){
                String uuid = UUID.randomUUID().toString();
                user.setApiKey(uuid);
                userRepository.save(user);
            }
        }
    }
}
