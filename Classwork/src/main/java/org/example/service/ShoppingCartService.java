package org.example.service;

import lombok.RequiredArgsConstructor;
import org.example.dto.ProductDto;
import org.example.dto.ShoppingCartDto;
import org.example.model.Product;
import org.example.model.Shopping_cart;
import org.example.repository.ProductRepository;
import org.example.repository.ShoppingCartRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ShoppingCartService {
    private static final Logger log = LoggerFactory.getLogger(ShoppingCartService.class);
    private final ShoppingCartRepository shoppingCartRepository;
    private final ProductRepository productRepository;
    private final RedisTemplate<Long,ShoppingCartDto> redisTemplate;
    public Shopping_cart createShoppingCart(String name){
        Shopping_cart shoppingCart = Shopping_cart.builder().name(name).build();
        ShoppingCartDto shoppingCartDto=ShoppingCartDto.builder().
                id(shoppingCart.getId()).
                name(shoppingCart.getName()).
                build();
        shoppingCart = shoppingCartRepository.save(shoppingCart);
        redisTemplate.opsForValue().set(shoppingCart.getId(),shoppingCartDto);
        return shoppingCart;

    }

    public Shopping_cart addProductToCart(Long id,Long productId){

        Shopping_cart shoppingCart = shoppingCartRepository.findById(id).orElseThrow(
                ()->new RuntimeException("Shopping cart not found"));
        Product product = productRepository.findById(productId).get();
        ProductDto productDto=ProductDto.builder().
                id(productId).
                name(product.getName()).
                price(product.getPrice()).
                category(product.getCategory()).
                description(product.getDescription()).
                build();
        ShoppingCartDto shoppingCartDto = ShoppingCartDto.builder().
                id(shoppingCart.getId()).
                products(Set.of(productDto)).
                name(shoppingCart.getName()).
                build();




        if(!shoppingCart.getProducts().contains(productId)){
             shoppingCart.getProducts().add(productRepository.findById(productId).get());
             shoppingCartRepository.save(shoppingCart);
         }
//        System.out.println(shoppingCartDto.getProducts());
        redisTemplate.opsForValue().set(shoppingCart.getId(),shoppingCartDto);

        return shoppingCart;
    }

    public void removeProductFromCart(Long id){
        Shopping_cart shoppingCart = shoppingCartRepository.findById(id).orElseThrow(
                ()->new RuntimeException("Shopping cart not found"));
        //        shoppingCart.getProducts().removeIf(product -> product.getId().equals(productId));

        redisTemplate.delete(id);
        shoppingCartRepository.delete(shoppingCart);
    }
    public Shopping_cart getShoppingCartById(Long id){
        ShoppingCartDto shoppingCartDto = redisTemplate.opsForValue().get(id);

        Shopping_cart shoppingCarten=null;
        if(shoppingCartDto!=null){
            Set<Product> producten = shoppingCartDto.getProducts().stream()
                    .map(productDto -> Product.builder().id(productDto.getId()).name(productDto.getName()).build())
                    .collect(Collectors.toSet());
          shoppingCarten = Shopping_cart.builder().id(id).
                    name(shoppingCartDto.getName()).products(producten).build();
        }

        if(shoppingCartDto == null){
            Shopping_cart shoppingCart = shoppingCartRepository.findById(id).get();

            Set<ProductDto> productDtos = shoppingCart.getProducts().stream()
                    .map(product -> ProductDto.builder().id(product.getId()).name(product.getName()).build())
                    .collect(Collectors.toSet());

            ShoppingCartDto shoppingCartDtoadd = ShoppingCartDto.builder().
                    id(shoppingCart.getId()).
                    name(shoppingCart.getName()).
                    products(productDtos).
                    build();
            redisTemplate.opsForValue().set(id,shoppingCartDtoadd);
            log.info("Get from DB {}", id);
            return shoppingCart;
        }else{
            log.info("Get from Cache {}", id);
            return shoppingCarten;
        }
    }



}
