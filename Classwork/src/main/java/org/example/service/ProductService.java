package org.example.service;

import lombok.RequiredArgsConstructor;
import org.example.dto.ProductCreateDto;
import org.example.dto.ProductDto;
import org.example.exception.NotFoundException;
import org.example.exception.ProductNotFoundException;
import org.example.model.Category;
import org.example.model.Product;
import org.example.repository.ProductProjection;
import org.springframework.stereotype.Service;
import org.example.repository.ProductRepository;

import java.util.ArrayList;
import java.util.List;

import static org.example.exception.ErrorCode.ENTITY_NOT_FOUND_0001;

@Service
@RequiredArgsConstructor
public class ProductService {
    private final ProductRepository repo;
    private final ProductRepository productRepository;

    public Long create(ProductCreateDto productdto) {

        List<Product> all = productRepository.findAll();

//        boolean match = all.stream().anyMatch(p -> p.getName() == productdto.getName());
//        if (match) {
//            throw new
//        }
        Product product = Product.builder()
                .name(productdto.getName())
                .price(productdto.getPrice())
                .description(productdto.getDescription())
                .category(productdto.getCategory())
                .build();
        repo.save(product);
       return product.getId();
    }
    public List<ProductDto> findByPrice(Integer beet1,Integer beet2) {
        List<Product> byPrice;
        if(beet1 == null && beet2==null) {
            byPrice = repo.findAll();
        }else{
            byPrice = repo.findByPriceGreaterThanEqualAndLessThanEqual(beet1, beet2);
        }

        if (byPrice.isEmpty()) {
            throw new NotFoundException(ENTITY_NOT_FOUND_0001,Product.class.getSimpleName());
        }
        List<ProductDto> pros = new ArrayList<>();
        for (Product pro: byPrice) {
            ProductDto productDto = ProductDto.builder()
                    .name(pro.getName())
                    .price(pro.getPrice())
                    .description(pro.getDescription())
                    .category(pro.getCategory())
                    .price(pro.getPrice())
                    .build();
            pros.add(productDto);
        }
        return pros;
    }
    public List<ProductProjection> findByCategory() {
        return repo.findByCategory();

    }
}
