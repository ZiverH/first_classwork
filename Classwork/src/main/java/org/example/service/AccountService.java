package org.example.service;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.example.model.Account;
import org.example.repository.AccountRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@AllArgsConstructor
@Slf4j
public class AccountService {

    private final AccountRepository accountRepository;
    @Transactional
    @SneakyThrows
    public void transfer(long from, long to, double amount){
        log.info("{} transfer account ",from);
        log.info("Thread {} - Getting  transfer account with id {}", Thread.currentThread().getName(), from);
        Account fromAccount = accountRepository.findById(from)
                .orElseThrow(() -> new RuntimeException("Account not found"));
        if (fromAccount.getBalance() < amount) {
            throw new RuntimeException("Not enough balance");
        }
        log.info("Sleeping...");
            Thread.sleep(10000);
        log.info("Thread {} - Getting accepting account with id {}", Thread.currentThread().getName(), to);
        Account toAccount = accountRepository.findById(to)
                .orElseThrow(() -> new RuntimeException("Account not found"));
        log.info("Thread {} - Changing balance of account {}", Thread.currentThread().getName(), from);
        fromAccount.setBalance(fromAccount.getBalance() - amount);
        log.info("Thread {} - Changing balance of account {}", Thread.currentThread().getName(), to);
        toAccount.setBalance(toAccount.getBalance() + amount);
        log.info("Thread {} - Saving account with id {}", Thread.currentThread().getName(), from);
        log.info("Thread {} - Saving account with id {}", Thread.currentThread().getName(), to);



    }

}
