package org.example.repository;

import org.example.model.Product;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface ProductRepository extends JpaRepository<Product,Long> {
    @Query(value = "select s from Product as s where s.price>=:beet1 and s.price<=:beet2")
    List<Product> findByPriceGreaterThanEqualAndLessThanEqual(int beet1, int beet2);

    @Query(value ="select s.category as category,count(s) as productCount from Product as s group by s.category")
    List<ProductProjection> findByCategory();

    @Override
    @EntityGraph(attributePaths = {"productDetail","categories"})
    Optional<Product> findById(Long id);
}
