package org.example.repository;

import jakarta.persistence.LockModeType;
import org.example.model.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;

import java.util.Optional;

public interface AccountRepository extends JpaRepository<Account,Long> {
            @Lock(LockModeType.OPTIMISTIC)
            Optional<Account> findById(long id);
}
