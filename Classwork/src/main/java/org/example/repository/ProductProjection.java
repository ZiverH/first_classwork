package org.example.repository;

import org.example.model.Category;

public interface ProductProjection {
    public String getProductCount();
    public Category getCategory();

}
