package org.example.repository;

import org.example.model.Shopping_cart;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface ShoppingCartRepository extends JpaRepository<Shopping_cart,Long> {

    @Override
//    @Query(nativeQuery = false,value = "select sh from Shopping_cart sh join fetch sh.products where sh.id=:id")
    @EntityGraph(attributePaths = {"products","products.productDetail","products.categories"})
    Optional<Shopping_cart> findById(Long id);

    @Query(nativeQuery = true,value = "delete from product_shopping_cart psh where psh.productId=:id")
    void deleteByProductId(Long id);


}
