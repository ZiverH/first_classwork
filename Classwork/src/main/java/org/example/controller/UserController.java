package org.example.controller;

import lombok.AllArgsConstructor;
import org.example.dto.UserDto;
import org.example.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/user")
@AllArgsConstructor

public class UserController {

    private static final Logger log = LoggerFactory.getLogger(UserController.class);
    private final UserService userService;
    @PostMapping()
    public void createUser(@RequestBody UserDto userDto) {
        userService.createUser(userDto);
    }

    @PostMapping("/login")
    public ResponseEntity<Map<String,String>> login(@RequestBody UserDto userDto) {
        return ResponseEntity.ok(userService.login(userDto));
    }
    @PostMapping("/refresh-token")
    public ResponseEntity<Map<String,String>> refreshToken(@RequestBody Map<String,String> refreshToken) {
        return ResponseEntity.ok(userService.refreshToken(refreshToken));
    }
    @GetMapping("/setuuid")
    public ResponseEntity<Void> setUUID() {
        userService.setUUID();
        return ResponseEntity.ok().build();
    }



}
