package org.example.controller;

import lombok.AllArgsConstructor;
import org.example.service.AccountService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@AllArgsConstructor
@RestController
@RequestMapping("transfer")
public class AccountController {
        private final AccountService accountService;

    @GetMapping("/{amount}")
    public void transfer1(@PathVariable Integer amount) {
        try{
            accountService.transfer(1, 2, amount);
        }catch (Exception e){
            System.out.println("EXCEPTION!!!\n"+ e.getMessage().toUpperCase());
        }
    }
}
