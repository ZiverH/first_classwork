package org.example.controller;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.example.dto.ProductCreateDto;
import org.example.dto.ProductDto;
import org.example.repository.ProductProjection;
import org.example.service.ProductService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/product")
public class ProductController {
    private final ProductService productService;
    @PostMapping()
    public ResponseEntity<ProductCreateDto> createProduct(@RequestBody @Valid ProductCreateDto productDto) {
      long id= productService.create(productDto);
        return ResponseEntity.created(URI.create("/products/"+id)).body(productDto);
    }
    @GetMapping( "all")
    public ResponseEntity<List<ProductDto>> getProduct(@RequestParam(required = false) Integer pricefrom, @RequestParam(required = false) Integer priceto) {
        List<ProductDto> dtos= productService.findByPrice(pricefrom,priceto);
        return ResponseEntity.ok(dtos);
    }
    @GetMapping("/category")
    public ResponseEntity<List<ProductProjection>> getCategory() {
        List<ProductProjection> dtos= productService.findByCategory();
        return ResponseEntity.ok(dtos);
    }



}
