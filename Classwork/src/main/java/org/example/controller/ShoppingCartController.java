package org.example.controller;

import lombok.RequiredArgsConstructor;
import org.example.model.Shopping_cart;
import org.example.service.ShoppingCartCacheService;
import org.example.service.ShoppingCartService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;

@RestController
@RequiredArgsConstructor
@RequestMapping("/shopping-carts")
public class ShoppingCartController {
    private final ShoppingCartService shoppingCartService;
    @PostMapping()
    public ResponseEntity<Shopping_cart> createShoppingCart(@RequestBody String name) {
        Shopping_cart shoppingCart = shoppingCartService.createShoppingCart(name);
            return ResponseEntity.created(URI.create("shopping-carts/" + shoppingCart.getId())).body(new Shopping_cart(shoppingCart.getId(), name,null));
    }
    @PostMapping("/{id}/product")
    public ResponseEntity<Shopping_cart> addProductToCart(@PathVariable Long id,@RequestBody Long product_id){
        Shopping_cart shoppingCart = shoppingCartService.addProductToCart(id, product_id);
        return ResponseEntity.ok(shoppingCart);
    }
    @GetMapping("/{id}")
    public ResponseEntity<Shopping_cart> getShoppingCartById(@PathVariable Long id) {
        Shopping_cart shoppingCart = shoppingCartService.getShoppingCartById(id);
        return ResponseEntity.ok(shoppingCart);
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteShoppingCart(@PathVariable Long id) {
        shoppingCartService.removeProductFromCart(id);
        return ResponseEntity.ok().build();
    }

}

