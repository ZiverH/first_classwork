package org.example.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.SignatureException;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.example.exception.ErrorResponseDto;
import org.example.repository.UserRepository;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import static org.example.exception.ErrorCode.ACCESS_TOKEN_EXPIRED;

@RequiredArgsConstructor
@Component
@Slf4j
public class AuthRequestFilter extends OncePerRequestFilter {

    private final AuthService authService;
    private final UserRepository repository;
//JWT
// eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJhZG1pbkBnbWFpbC5jb20iLCJuYW1lIjoiWml2ZXIiLCJzdGF0dXMiOiJhY3RpdmUiLCJpZCI6MSwiYXV0aG9yaXR5IjpbIlVTRVIiLCJBRE1JTiJdLCJpYXQiOjE3Mjg3MTc3NjIsImV4cCI6MTcyOTM3MjgwMH0.3M2z7UkIsvflhONHtiQ5vhP1qPzIajyW_r7WRKe8k2k
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response,
                                    FilterChain filterChain) throws ServletException, IOException {
        try {
            System.out.println("REQUEST");
            log.info("Incoming Request: Method: {}, URL: {}",
                    request.getMethod(), request.getRequestURL());

        Optional<Authentication> authOptional = Optional.empty();
        authOptional = authService.getAuthentication(request);
        if (authOptional.isPresent()) {
            SecurityContextHolder.getContext().setAuthentication(authOptional.get());}
        filterChain.doFilter(request, response);
            System.out.println("RESPONSE");
            log.info("Incoming Response: Status: {}",
                    response.getStatus());

        }catch (Exception ex){

            if(ex instanceof ExpiredJwtException){
                String refreshToken = request.getHeader("refresh-token");

                handleJwtExpiredException(request, response);

            }else{
                throw new JwtException("Invalid token");
            }

        }
}
    private void handleJwtExpiredException(HttpServletRequest httpServletRequest,
                                           HttpServletResponse httpServletResponse)
            throws IOException {
        ErrorResponseDto errorResponseDTO = ErrorResponseDto.builder()
                .data(new HashMap<>())
                .message("Expired")
                .path(httpServletRequest.getRequestURI())
                .requestLang(httpServletRequest.getHeader("Accept-Language"))
                .code(ACCESS_TOKEN_EXPIRED)
                .status(HttpServletResponse.SC_UNAUTHORIZED)
                .detail("Token expired.")
                .build();
        httpServletResponse.setContentType(MediaType.APPLICATION_JSON_VALUE);
        String jsonErrorResponse = new ObjectMapper().writeValueAsString(errorResponseDTO);
        httpServletResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        httpServletResponse.getWriter().write(jsonErrorResponse);
    }


}
