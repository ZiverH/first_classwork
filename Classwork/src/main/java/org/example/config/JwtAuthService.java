package org.example.config;

import io.jsonwebtoken.*;
import io.jsonwebtoken.security.MalformedKeyException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;

import java.security.SignatureException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class JwtAuthService implements AuthService {
    private static final Logger log = LoggerFactory.getLogger(JwtAuthService.class);
    private final BaseJwtService baseJwtService;
    @Override
    public Optional<Authentication> getAuthentication(HttpServletRequest request) {
        String header = request.getHeader("Authorization");
        if (header!=null && header.startsWith("Bearer ")) {
            String token = header.substring(7);
                Jws<Claims> claimsJws = baseJwtService.parseJwt(token);
                return Optional.of(getAuthentication(claimsJws.getPayload()));

        }
        return Optional.empty();
    }

    private Authentication getAuthentication(Claims payload) {
        List<String> authorities = (List) payload.get("authority");
        List<GrantedAuthority> authorityList;
        authorityList = authorities
                .stream()
                .map(role -> new SimpleGrantedAuthority(role))
                .collect(Collectors.toList());
        JwtCredential credentials = new ModelMapper().map(payload, JwtCredential.class);

        return new UsernamePasswordAuthenticationToken(null,credentials,authorityList);
    }
}
