package org.example.config;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import jakarta.annotation.PostConstruct;
import org.example.dto.UserDto;
import org.example.model.User;
import org.example.model.Authority;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.crypto.SecretKey;
import java.time.Duration;
import java.time.Instant;
import java.util.Date;

@Service
public class BaseJwtService {

    private SecretKey key;

    @Value("${spring.security.jwt-secret-key}")
    private String secretKey;

    @PostConstruct
    public void init() {
        byte[] keyBytes;
        keyBytes = Decoders.BASE64.decode(secretKey);
        key = Keys.hmacShaKeyFor(keyBytes);
    }

    public Jws<Claims> parseJwt(String token) {
        return Jwts.parser()
                .verifyWith(key)
                .build()
                .parseSignedClaims(token);
    }

    public String accessToken(User user) {
        return Jwts.builder()
                .subject(user.getUsername())
                .issuedAt(new Date())
                .expiration(Date.from(Instant.now().plus(Duration.ofMinutes(1))))
                .header()
                .add("tokenType", "access")
                .and()
                .claim("authority", user.getAuthorities().stream().map(Authority::getAuthority).toList())
                .signWith(key)
                .compact();
    }
    public String refreshToken(User user) {
        return Jwts.builder()
                .subject(user.getUsername())
                .issuedAt(new Date())
                .expiration(Date.from(Instant.now().plus(Duration.ofMinutes(3))))
                .header()
                .add("tokenType", "refresh")
                .and()
                .claim("authority", user.getAuthorities().stream().map(Authority::getAuthority).toList())
                .signWith(key)
                .compact();
    }

}
