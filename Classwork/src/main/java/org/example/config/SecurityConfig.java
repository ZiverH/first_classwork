package org.example.config;

import lombok.RequiredArgsConstructor;
import org.example.model.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import java.util.List;

@Configuration
@RequiredArgsConstructor
public class SecurityConfig {

    private final AuthRequestFilter authRequestFilter;
    private final ApiKeyFilter apiKeyFilter;


    @Bean
    SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http.cors(AbstractHttpConfigurer::disable);
        http.csrf(AbstractHttpConfigurer::disable);

        http.authorizeHttpRequests(auth->auth.requestMatchers("/user").permitAll());
        http.authorizeHttpRequests(auth->auth.requestMatchers("/user/*").permitAll());
        http.authorizeHttpRequests(auth->auth.requestMatchers("/user/setUUID").hasAuthority(Role.ADMIN.name()));
        http.authorizeHttpRequests(auth->auth.requestMatchers("/test/user").hasAnyAuthority(Role.USER.name(),Role.ADMIN.name()));
        http.authorizeHttpRequests(auth->auth.requestMatchers("/test/admin").hasAuthority(Role.ADMIN.name()));
        http.authorizeHttpRequests(auth -> auth.anyRequest().authenticated());
        http.addFilterBefore(apiKeyFilter, UsernamePasswordAuthenticationFilter.class);
        http.addFilterBefore(authRequestFilter, UsernamePasswordAuthenticationFilter.class);
        http.httpBasic(Customizer.withDefaults());
        return http.build();
    }
    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }



}
