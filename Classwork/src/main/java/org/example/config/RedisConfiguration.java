package org.example.config;

import org.example.dto.ShoppingCartDto;
import org.example.model.Shopping_cart;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

@Configuration
public class RedisConfiguration {
    @Bean
     RedisTemplate<Long, ShoppingCartDto> redisTemplate(RedisConnectionFactory connectionFactory){
        final RedisTemplate<Long, ShoppingCartDto> template = new RedisTemplate<>();
        template.setConnectionFactory(connectionFactory);
        var jackson2JsonRedisSerializer = new GenericJackson2JsonRedisSerializer();
        template.setKeySerializer(jackson2JsonRedisSerializer);
        template.setValueSerializer(jackson2JsonRedisSerializer);
        template.afterPropertiesSet();
        return template;
    }
}
