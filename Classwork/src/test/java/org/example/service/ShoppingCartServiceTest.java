package org.example.service;

import org.example.dto.ProductDto;
import org.example.dto.ShoppingCartDto;
import org.example.model.Category;
import org.example.model.Product;
import org.example.model.Shopping_cart;
import org.example.repository.ProductRepository;
import org.example.repository.ShoppingCartRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ShoppingCartServiceTest {


    @InjectMocks
    private ShoppingCartService shoppingCartService;

    @Mock
    private ShoppingCartRepository shoppingCartRepository;

    @Mock
    private ProductRepository productRepository;

    @Mock
    private RedisTemplate<Long, ShoppingCartDto> redisTemplate;

    @Mock
    private ValueOperations<Long, ShoppingCartDto> valueOperations;



    @Test
    void givenNameandCreateShoppingCartThenReturnSuccess() {
        // Arrange
        Shopping_cart shoppingCart = Shopping_cart.builder()
                .name("newCart")
                .build();

        long generatedId = 27L;
        shoppingCart.setId(generatedId);

        ShoppingCartDto shoppingCartDto = ShoppingCartDto.builder()
                .name(shoppingCart.getName())
                .build();

        when(shoppingCartRepository.save(any())).thenReturn(shoppingCart);
        when(redisTemplate.opsForValue()).thenReturn(valueOperations);

        // Act
        Shopping_cart result = shoppingCartService.createShoppingCart("newCart");

        // Assert

        assertThat("newCart").isEqualTo(result.getName());
        assertThat(generatedId).isEqualTo(result.getId());
        verify(valueOperations, times(1)).set(eq(generatedId), eq(shoppingCartDto));

    }

    @Test
    void givenProductIdAndAddToShoppingCartThenReturnSuccess() {
        // Arrange
        Long cartId = 26L;
        Long productId = 1L;

        Shopping_cart shoppingCart = Shopping_cart.builder()
                .id(cartId)
                .name("Card")
                .products(new HashSet<>())
                .build();

        Product product = Product.builder()
                .id(productId)
                .name("kitab")
                .price(100)
                .category(Category.KITAB)
                .description("kitab")
                .build();

        ShoppingCartDto shoppingCartDto = ShoppingCartDto.builder()
                .id(cartId)
                .name("Card")
                .products(Set.of(
                        ProductDto.builder()
                                .id(productId)
                                .name(product.getName())
                                .price(product.getPrice())
                                .category(product.getCategory())
                                .description(product.getDescription())
                                .build()
                ))
                .build();

        when(shoppingCartRepository.findById(cartId)).thenReturn(Optional.of(shoppingCart));
        when(productRepository.findById(productId)).thenReturn(Optional.of(product));
        when(shoppingCartRepository.save(any())).thenReturn(shoppingCart);

        when(redisTemplate.opsForValue()).thenReturn(valueOperations);

        // Act
        Shopping_cart shoppingCart1 = shoppingCartService.addProductToCart(cartId, productId);


        assertThat(cartId).isEqualTo(shoppingCart1.getId());
        assertTrue(shoppingCart1.getProducts().contains(product));

        verify(shoppingCartRepository,times(1)).save(any());
        verify(valueOperations, times(1)).set(eq(cartId), eq(shoppingCartDto));

    }
    @Test
    void givenProductIdAndAddToShoppingCartThenReturnException(){
        Long cartId = 26L;
        Long productId = 1L;
        when(shoppingCartRepository.findById(cartId)).thenReturn(Optional.empty());
        assertThatThrownBy(() -> shoppingCartService.addProductToCart(cartId, productId))
                .isInstanceOf(RuntimeException.class).
                hasMessageContaining("Shopping cart not found");
    }
    @Test
    void givenIdAndRemoveShoppingCartThenReturnSuccess(){
        Long cartId = 26L;
        Shopping_cart shoppingCart = Shopping_cart.builder().id(cartId).build();

        when(shoppingCartRepository.findById(anyLong())).thenReturn(Optional.of(shoppingCart));

        shoppingCartService.removeProductFromCart(cartId);

        assertThat(cartId).isEqualTo(shoppingCart.getId());
        verify(shoppingCartRepository, times(1)).delete(shoppingCart);
        verify(redisTemplate, times(1)).delete(cartId);
    }

    @Test
    void givenIdAndRemoveShoppingCartThenReturnException(){
        Long cartId = 26L;
        when(shoppingCartRepository.findById(anyLong())).thenReturn(Optional.empty());

        assertThatThrownBy(()->shoppingCartService.removeProductFromCart(cartId))
                .isInstanceOf(RuntimeException.class)
                .hasMessage("Shopping cart not found");
    }
    @Test
    void givenCartIdWhenShoppingCartExistsInCacheThenReturnFromCache() {
        // Arrange
        Long cartId = 26L;
        Set<ProductDto> productDtos = Set.of(ProductDto.builder().id(1L).name("Product1").build());
        ShoppingCartDto shoppingCartDto = ShoppingCartDto.builder()
                .id(cartId)
                .name("Cart1")
                .products(productDtos)
                .build();

        when(redisTemplate.opsForValue()).thenReturn(valueOperations);
        when(redisTemplate.opsForValue().get(anyLong())).thenReturn(shoppingCartDto);

        // Act
        Shopping_cart result = shoppingCartService.getShoppingCartById(cartId);
        // Assert
        assertThat(cartId).isEqualTo(result.getId());
        verify(redisTemplate.opsForValue(), times(1)).get(cartId);
    }

    @Test
    void givenCartIdWhenShoppingCartNOTExistsInCacheThenReturnFromCache(){
        Long cartId = 26L;
        Long productId = 1L;
        when(redisTemplate.opsForValue()).thenReturn(valueOperations);
        when(redisTemplate.opsForValue().get(cartId)).thenReturn(null);

        Shopping_cart shoppingCart = Shopping_cart.builder().products(
                Set.of(Product.builder().id(productId).build()))
                .id(cartId).build();
        ShoppingCartDto shoppingCartDto = ShoppingCartDto.builder().products(
                Set.of(ProductDto.builder().id(productId).build()))
                .id(cartId).build();
        when(shoppingCartRepository.findById(anyLong())).thenReturn(Optional.of(shoppingCart));
        when(redisTemplate.opsForValue()).thenReturn(valueOperations);

        //Act
        Shopping_cart result = shoppingCartService.getShoppingCartById(cartId);

        assertThat(cartId).isEqualTo(shoppingCart.getId());
        verify(redisTemplate.opsForValue(), times(1)).get(cartId);
        verify(redisTemplate.opsForValue(),times(1)).set(cartId,shoppingCartDto);
    }
}