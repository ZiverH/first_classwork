package org.example.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.example.model.Product;
import org.example.model.Shopping_cart;
import org.example.service.ShoppingCartService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.RestDocumentationExtension;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.List;
import java.util.Set;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@SpringBootTest
@ExtendWith({RestDocumentationExtension.class, SpringExtension.class})
class ShoppingCartControllerTest {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext context;

    @MockBean
    private ShoppingCartService shoppingCartService;

    @BeforeEach
    public void setup(RestDocumentationContextProvider restDocumentation) {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.context)
                .apply(documentationConfiguration(restDocumentation)
                        .uris()
                        .withScheme("https")
                        .withHost("shoppingCart.com")
                        .withPort(363))
                .build();
    }

    @Test
    void whenGivenNameAndCreateCart() throws Exception {
        Shopping_cart shoppingCart = Shopping_cart.builder()
                .name("CartTest")
                .build();

        when(shoppingCartService.createShoppingCart(any())).thenReturn(shoppingCart);

        mockMvc.perform(post("/shopping-carts")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(shoppingCart.getName())))
                .andExpect(status().isCreated());
        verify(shoppingCartService).createShoppingCart(any());
    }

    @Test
    void whenGivenCartIdAndProductIdThenProductToCart() throws Exception {
        Long cartId = 26L;
        Long productId = 1L;
        Product product = Product.builder().
                id(productId).
                name("ProductTest").
                price(20).
                build();
        Shopping_cart shoppingCart = Shopping_cart.builder()
                .id(cartId)
                .name("CartTest")
                .products(Set.of(product))
                .build();
        when(shoppingCartService.addProductToCart(anyLong(),anyLong())).thenReturn(shoppingCart);


        mockMvc.perform(post("/shopping-carts/" + cartId + "/product")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(productId))) // Include productId in the request body
                .andExpect(status().isOk());

    }

    @Test
    void whenGivenIdAndReturnCart() throws Exception{
        Shopping_cart shoppingCart = Shopping_cart.builder().id(1L).build();
        when(shoppingCartService.getShoppingCartById(any())).thenReturn(
                shoppingCart);

        mockMvc.perform(get("/shopping-carts/1").
                        contentType(MediaType.APPLICATION_JSON).
                        content(new ObjectMapper().writeValueAsString(shoppingCart))).
                andExpect(status().isOk());
    }
    @Test
    void whenGivenIdAndDeleteCart() throws Exception{
        Shopping_cart shoppingCart = Shopping_cart.builder().id(1L).build();
        when(shoppingCartService.getShoppingCartById(any())).thenReturn(
                shoppingCart);

        mockMvc.perform(delete("/shopping-carts/1"))
                .andExpect(status().isOk());
    }
}
